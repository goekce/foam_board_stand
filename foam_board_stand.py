import os

import cadquery as cq

WALL_THICKNESS = 10  # in mm
L = 50  # length
H = 20  # height

shape = (
    cq.Workplane()
    .box(WALL_THICKNESS, L, H, centered=(True, True, False), combine=True)
    .moveTo(0, 0)
    .box(L, WALL_THICKNESS, H, centered=(True, True, False), combine=True)
    .faces(">Z or >X or <X or >Y or <Y")
    .shell(1, kind="intersection")
)
cq.exporters.export(shape, f"{os.path.splitext(__file__)[0]}.stl")
