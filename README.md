# Foam board stand

For building labyrinths using foam boards. CAD model is parametrizable.

Exported model is in the artifacts.

Code is licensed under MIT license and the generated model under CC BY 4.0.
